/*
    Programme ecrit par  Emilie Paquin [PAQE19528909] et Lilly-Gabrielle Champagne [CHAL06589118]
    Dans le cadre du cours INF3105 - Deuxieme TP, indexation de code

    Tel que demande, indentation faite a 1 tab a l'aide de gedit

    Pour compiler : make
    Pour excuter  : ./tp2 codePourIndexer.cpp (optionnel: FichierDeSortie.txt)
*/
#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <sstream>
#include <map>
#include <cstring>
#include <iterator>

using namespace std;

#include "main.h"
#include "pile.h"



int main(int argc, char * argv[]){
	string motLu, nomFichier, fichierRetour="", phraseTemporaire="", noLigneTempo, motPrecedent;
    	int i=0, j=0,k=0, noLigne=1;
    	vector<string> motsReserves, codeVecteur, identificateur;
    	map<string,string> mots;
    	bool chaineCaracteres=false, dieze=false, lineChange=false, lineInclude=false, commentaire1=false, commentaire2=false, ifnd=false, includeTermine=false, caract=false;
    	char caractere;
    	ArbreAVL<string> arbreAVL;
    	ouvrireFichier(motsReserves);
    	if (argc < 2){
        	cout << "Veuillez entrer le nom du fichier code a analyser" << endl;
    	} else {
        	entreeStandard(nomFichier, fichierRetour, argc, argv);
         	codeALire(nomFichier, codeVecteur);
          	for (string phraseTemporaire : codeVecteur){

	           	if(!rechercheMotsReserve(phraseTemporaire, motsReserves) & !traitementDuCode(arbreAVL, dieze, lineChange, commentaire1, ifnd, commentaire2, lineInclude, chaineCaracteres, caract, noLigneTempo, motPrecedent, phraseTemporaire, noLigne)){
		
	                	ajouterAVL(arbreAVL, phraseTemporaire, noLigne);
		
	           	}
             
	              	motPrecedent=phraseTemporaire;
	}
     	if (fichierRetour.size()>1){

		afficherResFichier(arbreAVL, fichierRetour);

	} else {

          	afficherRes(arbreAVL);

        }


}

	return 0;
}


/*
  Methode qui traite le code ou il faut trouver des identificateurs. Traite les changements de lignes.
  @param identi   vector contenant les mots ajoutés
  @param motLu    mot à ajouter dans la map et le vecteur
  @param mots     Map contenant les lignes auxquels apparaissent les identificateurs et les identificateurs
  @param noLigne  Numéro de la ligne oû nous sommes rendu
*/
bool traitementDuCode(ArbreAVL<string> &arbreAVL, bool &dieze, bool &lineChange, bool &commentaire1, bool &ifnd, bool &commentaire2, bool &lineInclude, bool &chaineCaracteres, bool &caract, string &noLigneTempo, string &motPrecedent, string &phraseTemporaire, int &noLigne){
	bool sortie = true;
	
	if(phraseTemporaire=="\n"){
		++noLigne;
	}
	if (lineChange && isdigit(phraseTemporaire[0])){
        	noLigne=stoi(phraseTemporaire);
            lineChange = false;
	}else if(commentaire1 && phraseTemporaire=="\n"){
		commentaire1=false;
  	}else if(commentaire2 && phraseTemporaire=="/" && motPrecedent=="*"){
                commentaire2=false;
	}else if(lineInclude&& phraseTemporaire=="\n"){
		lineInclude=false;
	}else if(chaineCaracteres && phraseTemporaire==";"){
		chaineCaracteres=false;
	}else if(ifnd){
		ajouterAVL(arbreAVL, phraseTemporaire, noLigne);
		ifnd=false;
	}else if(caract && phraseTemporaire=="'"){
		caract=false;
	}else if(dieze){
                dieze=validerDieze(phraseTemporaire, motPrecedent, lineInclude, lineChange, commentaire1, commentaire2, ifnd);
	}else if(phraseTemporaire =="\'" || phraseTemporaire =="\"" ){
		validerChaineCaracteres(phraseTemporaire, chaineCaracteres, caract);
	}else if(phraseTemporaire == "#" || phraseTemporaire =="/" || phraseTemporaire =="*"){
		dieze=true;
	}
		sortie= dieze||caract||ifnd||chaineCaracteres||lineInclude||commentaire2||commentaire1;
	return sortie;
}


/*
  Methode qui ajoute les identificateurs à l'arbre
  @param arbreAVL              structure de l'arbre
  @param phraseTemporaire      mot à ajouter dans l'arbre
  @param noLigne    Numéro de la ligne oû nous sommes rendu
*/
void ajouterAVL(ArbreAVL<string> &arbreAVL, string &phraseTemporaire, int &noLigne){

   
	bool entrer=true;
	entrer=isalpha(phraseTemporaire[0]); 
	if(entrer){
		if (arbreAVL.contient(phraseTemporaire)){
			arbreAVL.insererNoLigne(phraseTemporaire, noLigne);
		}else {
			arbreAVL.inserer(phraseTemporaire, noLigne);
		}
	}
}


/*
  Methode qui écrit dans un fichier de sortie
  @param arbreAVL     structure d'arbre AVL a afficher 
  @param nomDuFichier nom du fichier ou écrire le résultat
*/
void afficherResFichier(ArbreAVL<string> &arbreAVL, string nomDuFichier){
	ArbreAVL<string>::Iterateur iter = arbreAVL.debut();
	ofstream sortie(nomDuFichier.c_str(), ios::out | ios::trunc);
	if(sortie){
		for(iter; iter != arbreAVL.fin(); ++iter){
			string element = arbreAVL[iter];
			sortie << element << ": ";
        		vector<int> lignes = arbreAVL.sortie(element);
        		for(vector<int>::iterator it = lignes.begin(); it != lignes.end(); it++){
            			if(lignes.size()>1 && it != lignes.end()-1){
              				sortie << *it << ", ";
            			}else{
              				sortie << *it;
            			}
        		}
        		sortie << endl;
    		}
		sortie.close();
	}else{
	cout<<"Impossible d'ouvrire le fichier de sortie"<<endl;
	}
    
}


/*
  Methode qui affiche l'arbre contenant les identificateurs et leur numero de ligne
  @param arbreAVL structure d'arbre AVL a afficher 
*/
void afficherRes(ArbreAVL<string> &arbreAVL){

	ArbreAVL<string>::Iterateur iter = arbreAVL.debut();
	for(iter; iter != arbreAVL.fin(); ++iter){
		string element = arbreAVL[iter];
		cout << element << ": ";
		vector<int> lignes = arbreAVL.sortie(element);
        	for(vector<int>::iterator it = lignes.begin(); it != lignes.end(); it++){
            		if(lignes.size()>1 && it != lignes.end()-1){
              		cout << *it << ", ";
            		}else{
              			cout << *it;
            		}		
        	}
		cout << endl;
	}

}


/*
  Initialisation d'un vecteur contenant chacune des lignes du fichier de code à traiter
  @param nomFichier   Nom du fichier à ouvrire pour récupérer le code
  @param codeVecteur  Vecteur qui contiendra chacune des lignes
*/
void codeALire(string nomFichier, vector<string> &codeVecteur){
	char caractere;
	string tempo, motTemp;
	ifstream code(nomFichier.c_str(), ios::in);
	if(code){
		while(code.get(caractere)){
			if(!isalnum(caractere) && caractere!='_' && caractere!='-'){
				if(tempo.size() != 0){
					codeVecteur.push_back(tempo);
					tempo.clear();
				}
				motTemp=(1,caractere);
				codeVecteur.push_back(motTemp);
			}else{
			tempo=tempo+""+caractere;
			}
		}
	}
	code.close();
}


/*
  Recherche du mot en traitement dans la liste des mots réservés
  @param motLu        Mot en traitement
  @param motsReserves Vecteur contenant la liste des mots réservés
  @return retour      retourne vrai si le mot traité est réservé et faux si non
*/
bool rechercheMotsReserve(string motLu, vector<string> &motsReserves){
	bool reserve = false;
	for (string s : motsReserves){
		if (s == motLu){
			reserve = true;
		}
 	}
	return reserve;
}


/*
  Initialisation du vecteur qui contiendra la liste des mots réservés
  @param motsReserves Vecteur contenant la liste des mots réservés
*/
void ouvrireFichier(vector<string> &motsReserves){

	ifstream listeDeMots("motsReserves.txt");

	if(listeDeMots){

		string ligne;
		while (getline(listeDeMots, ligne)){
			motsReserves.push_back (ligne);
		}

	} else {
		cout << "Validez que le fichier motsReserves.txt est dans le meme repertoire que l'executable." << endl;
	}
}


/*
  Cette méthode va chercher les éléments entrés sur l'entrée standard et initialise les variables avec leur valeur
  @param nomFichier    Nom du fichier de codes à traiter
  @param fichierRetour Nom du fichier de retour (optionel)
  @param taille        Nombre d'arguments sur l'entrée standard
  @param tabEntree     Tableau des valeurs entrées sur l'entrée standard
*/
void entreeStandard(string &nomFichier, string &fichierRetour, int taille, char * tabEntree[]){
	int i, grandeur;
	string phrase;
	for(i=0; i<taille; i++){
		phrase=tabEntree[i];
		grandeur=phrase.size();
		if(phrase.substr(grandeur-4, grandeur-1)==".cpp"){
			nomFichier=phrase;
		}else if(phrase.substr(grandeur-4, grandeur-1)==".txt"){
			fichierRetour=phrase;
		}
	}
}


/*
  Traitement des variables contenant un #
  @param motLu         Mot en traitement
  @param lineInclude   Vrai si le mot traite est #include faux si non
  @param lineChange    Vrai si le mot traite est #line faux si non
*/
bool validerDieze(const string motLu, const string motPrecedent, bool &lineInclude, bool &lineChange, bool &commentaire1, bool &commentaire2, bool &ifnd){
	bool retour=false;
	if (motLu == "include"){
		lineInclude = true;
	}else if(motLu == "IFNDEF" || motLu == "ifndef" || (motLu == "defined" && motPrecedent == "!") || (motLu == "IF !DEFINED"&& motPrecedent == "!")){
		ifnd=true;
	}else if(motLu == "*" && motPrecedent == "/"){
		commentaire2=true;
	}else if(motLu == "/" && motPrecedent == "/"){
		commentaire1=true;
    	}
    	if (motLu == "ligne" || motLu== "line"){
        	lineChange = true;
    	}
    retour=lineInclude||ifnd||commentaire2||commentaire1||lineChange;
    return retour; 
}


/*
  Traitement des commentaires
*/
void validerChaineCaracteres(const string motLu, bool &chaineCaracteres, bool &caract){
	if(motLu == "\"" && !chaineCaracteres){
		chaineCaracteres=true;
	}else if(motLu == "\'" && !caract && !chaineCaracteres){
		caract=true;
	}
}


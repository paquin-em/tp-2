#ifndef HEADER_MAIN
#define HEADER_MAIN
#include "arbreAVL.h"

void ajouterAVL(ArbreAVL<string> &arbreAVL, string &phraseTemporaire, int &noLigne);
void afficherRes(ArbreAVL<string> &arbreAVL);
bool traitementDuCode(ArbreAVL<string> &arbreAVL, bool &dieze, bool &lineChange, bool &commentaire1, bool &ifnd, bool &commentaire2, bool &lineInclude, bool &chaineCaracteres, bool &caract, string &noLigneTempo, string &motPrecedent, string &phraseTemporaire, int &noLigne);
void validerChaineCaracteres(const string motLu, bool &chaineCaracteres, bool &caract);
void validerMot(vector<string> identi, const string motLu, map<string, string> &mots, bool &ifnd, int &noLigne);
bool validerDieze(const string motLu, const string motPrecedent, bool &lineInclude, bool &lineChange, bool &commentaire1, bool &commentaire2, bool &ifnd);
void validerFinLigne(char x, int &noLigne);
void codeALire(string nomFichier, vector<string> &codeVecteur);
void entreeStandard(string &nomFichier, string &fichierRetour, int taille, char * tabEntree[]);
void ouvrireFichier(vector<string> &motsReserves);
bool rechercheMotsReserve(string motLu, vector<string> &motsReserves);
void afficherResFichier(ArbreAVL<string> &arbreAVL, string nomDuFichier);


#endif  // HEADER_MAIN

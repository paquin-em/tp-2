# Programme ecrit par  Emilie Paquin [PAQE19528909] et Lilly-Gabrielle Champagne [CHAL06589118]
# Dans le cadre du cours INF3105 - Deuxieme TP, indexation de code


all: tp2

tp2: main.cpp main.h pile.h arbreAVL.h
	g++ -o tp2 main.cpp -lm -std=c++11

clean: 
	rm -f tp2.o
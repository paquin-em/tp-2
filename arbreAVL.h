/*
    Programme ecrit par  Emilie Paquin [PAQE19528909] et Lilly-Gabrielle Champagne [CHAL06589118]
    Dans le cadre du cours INF3105 - Deuxieme TP, indexation de code

*/
#if !defined(__ARBREAVL__H__)
#define __ARBREAVL__H__
#include <assert.h>
#include <stdio.h>
#include <vector>
#include "pile.h"


template <class T>
class ArbreAVL
{
  public:
    //Constructeurs et destructeur.
    ArbreAVL();
    ArbreAVL(const ArbreAVL&);
    ~ArbreAVL();

    //Fonction pour manipuler l'arbre.
    bool contient(const T&) const;
    void inserer(const T&, int &noLigne);
    void insererNoLigne(const T& element, int &noLigne);
    bool vide() const;
    void vider();
    vector<int> sortie(const T& element);

    // Annonce l'existance d'une classe Iterateur.
    class Iterateur;

    // Fonction pour obtenir un itérateur (position dans l'arbre)
    Iterateur debut() const;
    Iterateur fin() const;
    Iterateur rechercher(const T&) const;
    Iterateur rechercherEgalOuSuivant(const T&) const;
    Iterateur rechercherEgalOuPrecedent(const T&) const;

    // Accès aux éléments de l'arbre via un intérateur.
    const T& operator[](const Iterateur&) const;
    T& operator[](const Iterateur&);

    // Copie d'un arbre AVL.
    ArbreAVL& operator = (const ArbreAVL&);

    int noLigne;

  private:
    struct Noeud{
        Noeud(const T&);
        T contenu;
        int equilibre;
        vector<int> ligne;
        Noeud *gauche;
        Noeud *droite;
    };
    Noeud* racine;

    // Fonctions internes.
    bool inserer(Noeud*&, const T&, int noLigne);
    void rotationGaucheDroite(Noeud*&);
    void rotationDroiteGauche(Noeud*&);
    void vider(Noeud*&);
    void copier(const Noeud*, Noeud*&) const;


  public:
    // Sera présenté à la semaine #7
    class Iterateur{
      public:
        Iterateur(const ArbreAVL& a);
        Iterateur(const Iterateur& a);
        Iterateur(const ArbreAVL& a, Noeud* c);
        Iterateur(ArbreAVL& a, Noeud* c);

        operator bool() const;
        bool operator!() const;
        bool operator==(const Iterateur&) const;
        bool operator!=(const Iterateur&) const;

        const T& operator*() const;

        Iterateur& operator++();  // A IMPLEMENTER
        Iterateur operator++(int);
        Iterateur& operator = (const Iterateur&);
      private:
        const ArbreAVL& arbre_associe;
        Noeud* courant;
        Pile<Noeud*> chemin;

      friend class ArbreAVL;
    };
};


//-----------------------------------------------------------------------------

template <class T>
ArbreAVL<T>::Noeud::Noeud(const T& c):
gauche(NULL), droite(NULL), equilibre(0), contenu(c)
{
}

template <class T>
ArbreAVL<T>::ArbreAVL() : racine(NULL)
{
}

template <class T>
ArbreAVL<T>::ArbreAVL(const ArbreAVL<T>& autre) : racine(NULL)
{
    copier(autre.racine, racine);
}

template <class T>
ArbreAVL<T>::~ArbreAVL()
{
    vider(racine);
}

template <class T>
bool ArbreAVL<T>::contient(const T& element) const
{
    Noeud *temp = this->racine;
    while (temp != NULL){
	if (this->racine->contenu == element){
	    return true;
	}else if (this->racine->contenu < element){
		temp = temp->droite;
	}else{
		temp = temp->gauche;
	}
    return false;
    }

}

template <class T>
void ArbreAVL<T>::inserer(const T& element, int &noLigne)
{
    inserer(racine, element, noLigne);
}

template <class T>
void ArbreAVL<T>::insererNoLigne(const T& element, int &noLigne){

    Iterateur iter = rechercher(element);
    iter.courant->ligne.push_back(noLigne);

}

template <class T>
vector<int> ArbreAVL<T>::sortie(const T& element){

    vector<int> sortie;

    Iterateur iter = rechercher(element);
    sortie = iter.courant->ligne;
    return sortie;

}

template <class T>
bool ArbreAVL<T>::inserer(Noeud*& noeud, const T& element, int noLigne)
{
    if(noeud==NULL)
    {
        noeud = new Noeud(element);
        noeud->ligne.push_back(noLigne);
        return true;
    }
    if(element < noeud->contenu){
        if(inserer(noeud->gauche, element, noLigne))
        {
            noeud->equilibre++;
            if(noeud->equilibre == 0) return false;
            if(noeud->equilibre == 1) return true;
            assert(noeud->equilibre == 2);
            if(noeud->gauche->equilibre == -1)
                rotationDroiteGauche(noeud->gauche);
            rotationGaucheDroite(noeud);
        }
        return false;
    }
    else if(noeud->contenu < element){
        if(inserer(noeud->droite, element, noLigne))
        {
            noeud->equilibre--;
            if(noeud->equilibre == 0) return false;
            if(noeud->equilibre == -1) return true;
            assert(noeud->equilibre== -2);
            if(noeud->droite->equilibre == 1)
                rotationGaucheDroite(noeud->droite);
            rotationDroiteGauche(noeud);
        }
        return false;
    }
    noeud->contenu = element;
    noeud->ligne.push_back(noLigne);
    return false;
}

template <class T>
void ArbreAVL<T>::rotationGaucheDroite(Noeud*& racinesousarbre)
{
    Noeud *temp = racinesousarbre->gauche;
    int  ea = temp->equilibre;
    int  eb = racinesousarbre->equilibre;
    int  neb = -(ea>0 ? ea : 0) - 1 + eb;
    int  nea = ea + (neb < 0 ? neb : 0) - 1;

    temp->equilibre = nea;
    racinesousarbre->equilibre = neb;
    racinesousarbre->gauche = temp->droite;
    temp->droite = racinesousarbre;
    racinesousarbre = temp;
}

template <class T>
void ArbreAVL<T>::rotationDroiteGauche(Noeud*& racinesousarbre)
{
    Noeud *temp = racinesousarbre->droite;
    int  eb = temp->equilibre;
    int  ea = racinesousarbre->equilibre;
    int  nea = -(eb<0 ? eb : 0) +1 + ea;
    int  neb = (nea>0 ? nea : 0) +1 + eb;

    temp->equilibre = neb;
    racinesousarbre->equilibre = nea;
    racinesousarbre->droite = temp->gauche;
    temp->gauche = racinesousarbre;
    racinesousarbre = temp;

}

template <class T>
bool ArbreAVL<T>::vide() const
{
    if (this->racine != NULL){
	return false;
    }
    return true;
}

template <class T>
void ArbreAVL<T>::vider(){
  vider(racine);
}

template <class T>
void ArbreAVL<T>::vider(Noeud*& noeud)
{

    if(noeud != NULL) {
        vider(noeud->gauche);
        vider(noeud->droite);
        delete noeud;
        noeud = NULL;
    }

}

template <class T>
void ArbreAVL<T>::copier(const Noeud* source, Noeud*& noeud) const
{
    if(source != NULL){
	noeud= new Noeud(source->contenu);
	noeud->equilibre = source->equilibre;
	copier(source->gauche, noeud->gauche);
	copier(source->droite, noeud->droit);
    }
}


template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::debut() const
{
    Iterateur iter(*this);
    iter.courant = this->racine;
    if (iter.courant != NULL){
        while (iter.courant->gauche != NULL){
            iter.chemin.empiler(iter.courant);
            iter.courant = iter.courant->gauche;
        }
    }

    return iter;
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::fin() const
{
    return Iterateur(*this);
}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercher(const T& e) const
{

    Iterateur iter(*this);
    iter.courant=this->racine;
    Noeud * temp = racine;
    while(temp){
        if(temp->contenu==e){
            iter.courant = temp;
            temp=NULL;
        }else if(e<temp->contenu){
            iter.chemin.empiler(temp);
            temp=temp->gauche;
        }else{
            temp=temp->droite;
        }
    }
    if (iter.courant != NULL)
        iter.chemin.vider();
   
    return iter;

}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercherEgalOuSuivant(const T& e) const
{

    Iterateur iter(*this);
    bool sortie = true;
    iter.courant=this->racine;
    while(iter.courant!=NULL && sortie){
        if(iter.courant->contenu==e){
            sortie=false;
        }else if(e<iter.courant->contenu){
            iter.chemin.empiler(iter.courant);
            iter.courant=iter.courant->gauche;
        }else{
            iter.courant=iter.courant->droite;
        }
    }
    if(iter.courant==NULL){
        iter.courant=iter.chemin.depiler();
    }
    return iter;

}

template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::rechercherEgalOuPrecedent(const T& e) const
{
    Iterateur iter(*this);
    bool sortie = true;
    Noeud*dernier=NULL;
    iter.courant=this->racine;
    while(iter.courant!=NULL && sortie){
        if(iter.courant->contenu==e){
            sortie=false;
        }else if(e<iter.courant->contenu){
            iter.chemin.empiler(iter.courant);
            iter.courant=iter.courant->gauche;
        }else{
            dernier=iter.courant;
            iter.courant=iter.courant->droite;
        }
    }
    if(iter.courant==NULL){
        iter.courant=dernier;
    }
    return iter;
}

template <class T>
const T& ArbreAVL<T>::operator[](const Iterateur& iterateur) const
{
    assert(&iterateur.arbre_associe == this);
    assert(iterateur.courant);
    return iterateur.courant->contenu;
}

template <class T>
T& ArbreAVL<T>::operator[](const Iterateur& iterateur)
{
    assert(&iterateur.arbre_associe == this);
    assert(iterateur.courant);
    return iterateur.courant->contenu;
}

template <class T>
ArbreAVL<T>& ArbreAVL<T>::operator=(const ArbreAVL& autre) {
    if(this==&autre) return *this;
    vider();
    copier(autre.racine, racine);
    return *this;
}

//------------------------------
template <class T>
ArbreAVL<T>::Iterateur::Iterateur(const ArbreAVL& a)
 : arbre_associe(a), courant(NULL)
{
}

template <class T>
ArbreAVL<T>::Iterateur::Iterateur(const ArbreAVL<T>::Iterateur& a)
: arbre_associe(a.arbre_associe)
{
    courant = a.courant;
    chemin = a.chemin;
}

// Pré-incrément
template <class T>
typename ArbreAVL<T>::Iterateur& ArbreAVL<T>::Iterateur::operator++()
{

    if(courant->droite != NULL) {
        courant = courant->droite;
        while(courant->gauche != NULL) {
           chemin.empiler(courant);
           courant = courant->gauche;
        }

    }else if(!chemin.vide()){
      courant= chemin.depiler();
    }else{
        courant=NULL;
    }

    return *this;

}

// Post-incrément
template <class T>
typename ArbreAVL<T>::Iterateur ArbreAVL<T>::Iterateur::operator++(int)
{
    Iterateur copie(*this);
    operator++();
    return copie;
}

template <class T>
ArbreAVL<T>::Iterateur::operator bool() const
{
    return courant!=NULL;
}

template <class T>
bool ArbreAVL<T>::Iterateur::operator!() const{
    return courant==NULL;
}

template <class T>
bool ArbreAVL<T>::Iterateur::operator==(const Iterateur& o) const{
    assert(&arbre_associe==&o.arbre_associe);
    return courant==o.courant;
}

template <class T>
bool ArbreAVL<T>::Iterateur::operator!=(const Iterateur& o) const{
    assert(&arbre_associe==&o.arbre_associe);
    return courant!=o.courant;
}

template <class T>
const T& ArbreAVL<T>::Iterateur::operator *() const
{
    assert(courant!=NULL);
    return courant->contenu;
}

template <class T>
typename ArbreAVL<T>::Iterateur& ArbreAVL<T>::Iterateur::operator = (const Iterateur& autre){
    assert(&arbre_associe==&autre.arbre_associe);
    courant = autre.courant;
    chemin = autre.chemin;
    return *this;
}

#endif

